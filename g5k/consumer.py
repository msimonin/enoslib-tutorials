#!/usr/bin/env python
import sys
import time

import pika


# defining what to do when a message is received
def callback(ch, method, properties, body):
    body_str = body.decode("utf8").replace("'", '"')
    number = body_str

    channel.basic_publish(
        exchange="", routing_key="received_messages", body=str(number)
    )

    time.sleep(1.0)

    # Manually acknowledge the message
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    global idx

    host = sys.argv[1]

    # getting a connection to the broker
    parameters = pika.ConnectionParameters(host, 5672)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    # declaring the queue again (to be sure)
    channel.queue_declare(queue="fault_injection")
    # auto_ack: as soon as collected, a message is considered as acked
    channel.basic_consume(queue="fault_injection", auto_ack=False, on_message_callback=callback)

    # declaring the queue for received messages
    # no messages will be processed !
    # the only purpose of this is to retrieve the total number of messages processed by the first queue
    channel.queue_declare(queue="received_messages")

    # wait for messages
    channel.start_consuming()
