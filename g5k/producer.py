#!/usr/bin/env python
import random
import sys
import time

import pika

if __name__ == "__main__":
    host = sys.argv[1]

    # getting a connection to the broker
    parameters = pika.ConnectionParameters(host, 5672)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    # declaring the queue
    channel.queue_declare(queue="fault_injection")
    # send the message, through the exchange ''
    # which simply delivers to the queue having the key as name
    while 1:
        number = random.randint(0, 100)
        channel.basic_publish(
            exchange="", routing_key="fault_injection", body=str(number)
        )
        
        time.sleep(1.0)

    # gently close (flush)
    connection.close()
