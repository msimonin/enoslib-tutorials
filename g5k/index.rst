Jupyter + EnOSlib + Grid'5000 = 💖
===================================


.. toctree::
    :maxdepth: 1
    :caption: Contents:

    01_remote_actions_and_variables.ipynb
    02_environment_control_resource_selection.ipynb
    03_observability_service.ipynb
    04_network_emulation.ipynb
    05_using_several_networks.ipynb
    06_working_with_virtualized_resources.ipynb
    07_orchestrators.ipynb
    08_planning_service.ipynb
    09_planning_service_revisited.ipynb